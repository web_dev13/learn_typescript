const names: string[] =[];
names.push("Dylan");
names.push("Nim");
console.log(names[0]);
console.log(names[1]);
console.log(names.length);

console.log("Format 1 :");
for(let i=0; i<names.length;i++){
    console.log(names[i]);    
}

console.log("Format 2 :");
for(let ind in names){
    console.log(names[ind]);
}

console.log("Format 3 :");
names.forEach(function(name){  
    console.log(name);

});

